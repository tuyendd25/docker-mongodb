FROM ubuntu:18.04

MAINTAINER tuyendd

RUN apt-get update && apt-get install -y wget && apt-get install -y gnupg2 && apt-get install -y apt-utils

RUN wget -qO - https://www.mongodb.org/static/pgp/server-4.0.asc | apt-key add -

RUN touch /etc/apt/sources.list.d/mongodb-org-4.0.list && echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.0.list

RUN apt-get update

RUN apt-get install -y mongodb-org

RUN mkdir -p /data/db

EXPOSE 27017

CMD ["--port 27017", "--smallfiles"]

ENTRYPOINT usr/bin/mongod
